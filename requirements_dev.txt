# quantify

pip
bump2version==0.5.11
wheel==0.33.6
watchdog==0.9.0
coverage
sphinx>=2,<4 # jupyter_sphinx requires sphinx>=2, <4 avoids a sphinx warning from jupyter_sphinx
sphinx-rtd-theme==0.5.0rc2
sphinxcontrib-bibtex
jupyter_sphinx>=0.3.2
sphinx-jsonschema>=1.15
sphinx-autobuild
sphinx-togglebutton
sphinx-autodoc-typehints>=1.12.0
twine==1.14.0
pytest
pytest-runner
pytest-cov
pytest-mpl
pytest-mock
pytest-xdist # run parallel tests
pygments==2.6.1

black
mypy
types-docutils
pylint
pre-commit
pre-commit-hooks # common hooks for git repos
isort>=5.10.0 # sort python imports

scanpydoc >=0.7.1, <=0.7.3 # ensures latest typing_extensions is used in sphinx (https://github.com/theislab/scanpydoc/issues/31)
docutils<0.17 # pinned due to issue #175 (quantify-core)
rich[jupyter]
jupytext

# scheduler-only

cachecontrol # see issue quantify-scheduler#129
lockfile # see issue quantify-scheduler#129
pytest-mpl

# Prospector
prospector
prospector[with_mypy]
